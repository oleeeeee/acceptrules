package nl.lolmewn.acceptrules;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.*;

import java.util.Iterator;

/**
 * @author Lolmewn
 */
public class Events implements Listener {

    private final Main plugin;
    private final String ARCOMMAND = "/acceptrules", RULESCOMMAND = "/rules";

    public Events(Main m) {
        plugin = m;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void beforeCommand(PlayerCommandPreprocessEvent event) {
        if (plugin.getConfig().getBoolean("BlockCommandsBeforeAccept", true)) {
            if (plugin.getAcceptedPlayers().contains(event.getPlayer().getUniqueId().toString())) {
                return;
            }
            String cmd = event.getMessage().contains(" ") ? event.getMessage().split(" ")[0] : event.getMessage();
            if (!cmd.equalsIgnoreCase(RULESCOMMAND) && !cmd.equalsIgnoreCase(ARCOMMAND)) {
                event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',
                        plugin.getConfig().getString("InformMsg",
                                "'&6[AcceptRules] &cYou have to accept the &6Rules&c! Use &b/Rules &cand then &b/acceptRules&c!'")));
                event.setCancelled(true);
            }
            //let commands handle it themselves.
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (plugin.getAcceptedPlayers().contains(event.getPlayer().getUniqueId().toString())) {
            return;
        }
        if (plugin.getConfig().getBoolean("TpOnJoin", false)) {
            World w = plugin.getServer().getWorld(plugin.getConfig().getString("SpawnWorld"));
            if (w == null) {
                w = plugin.getServer().getWorlds().get(0);
            }
            Location loc = new Location(w,
                    plugin.getConfig().getDouble("SpawnPositionX", 0),
                    plugin.getConfig().getDouble("SpawnPositionY", 0),
                    plugin.getConfig().getDouble("SpawnPositionZ", 0),
                    plugin.getConfig().getLong("SpawnPositionYaw", 0),
                    plugin.getConfig().getLong("SpawnPositionPitch", 0));
            event.getPlayer().teleport(loc, PlayerTeleportEvent.TeleportCause.PLUGIN);
        }
        if (plugin.getConfig().getBoolean("InformUser", true)) {
            plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
                @Override
                public void run() {
                    event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',
                            plugin.getConfig().getString("InformMsg",
                                    "&6[AcceptRules] &cYou have to accept the &6Rules&c! Use &b/Rules &cand then &b/acceptRules&c!")));
                }
            }, 10L);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (plugin.getAcceptedPlayers().contains(event.getPlayer().getUniqueId().toString())) {
            return;
        }
        if (!plugin.getConfig().getBoolean("AllowBuildBeforeAccept", false)) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',
                    plugin.getConfig().getString("CantBuildMsg", "&6[AcceptRules] &cYou have to accept the &6Rules &cto build! Use &b/Rules &cand then &b/acceptRules&c!")));
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (plugin.getAcceptedPlayers().contains(event.getPlayer().getUniqueId().toString())) {
            return;
        }
        if (!plugin.getConfig().getBoolean("AllowBuildBeforeAccept", false)) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',
                    plugin.getConfig().getString("CantBuildMsg", "&6[AcceptRules] &cYou have to accept the &6Rules &cto build! Use &b/Rules &cand then &b/acceptRules&c!")));
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (plugin.getAcceptedPlayers().contains(event.getPlayer().getUniqueId().toString())) {
            return;
        }
        if (plugin.getConfig().getBoolean("AllowMoveBeforeAccept", true)) {
            return;
        }
        event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onChat(AsyncPlayerChatEvent event) {
        if (!plugin.getConfig().getBoolean("HideChatBeforeAccept", true)) {
            return;
        }
        if (!plugin.getAcceptedPlayers().contains(event.getPlayer().getUniqueId().toString())) {
            //he's trying to send a message, but can't receive messages anyway. Let's cancel and inform!
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&',
                    plugin.getConfig().getString("CantChatMsg",
                            "'&6[AcceptRules] &cYou have to accept the &6Rules &cto chat! Use &b/Rules &cand then &b/acceptRules&c!'")));
            event.setCancelled(true);
            return;
        }
        Iterator<Player> it = event.getRecipients().iterator();
        while (it.hasNext()) {
            Player player = it.next();
            if (!plugin.getAcceptedPlayers().contains(player.getUniqueId().toString())) {
                it.remove();
            }
        }
    }

}
