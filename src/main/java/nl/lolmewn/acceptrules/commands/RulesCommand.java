package nl.lolmewn.acceptrules.commands;

import nl.lolmewn.acceptrules.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * @author Lolmewn
 */
public class RulesCommand implements CommandExecutor {

    private final Main plugin;
    private final HashMap<UUID, Long> lastCommand = new HashMap<>();

    public RulesCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can only use this command as a player!");
            return true;
        }
        Player player = (Player) sender;
        if (!plugin.getAcceptedPlayers().contains(player.getUniqueId().toString())
                && lastCommand.containsKey(player.getUniqueId())
                && System.currentTimeMillis() - lastCommand.get(player.getUniqueId()) - plugin.getConfig().getDouble("timeBetweenRules", 0) * 1000 < 0) {
            player.sendMessage(ChatColor.YELLOW + "Please take your time to read the rules first before going to the next page!");
            return true;
        }
        if (plugin.getConfig().getString("rulesTitle") != null && !"".equals(plugin.getConfig().getString("rulesTitle"))) {
            player.sendMessage(ChatColor.YELLOW + ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("rulesTitle", "====Rules====")));
        }
        if (!plugin.getConfig().getBoolean("usePagination", true)) {
            for (final String page : plugin.getRulesManager().keySet()) {
                this.readPage(player, page);
            }
            this.lastCommand.put(player.getUniqueId(), System.currentTimeMillis());
            return true;
        }
        try {
            final String page = plugin.getRulesManager().size() != 1 && args.length > 0 ?
                    args[0] : plugin.getRulesManager().keySet().iterator().next();
            if (!plugin.getRulesManager().containsKey(page)) {
                sender.sendMessage(ChatColor.RED + "No page " + page + "!");
                StringBuilder pages = new StringBuilder(ChatColor.RED + "Available pages: ");
                for (String p : plugin.getRulesManager().keySet()) {
                    pages.append(p).append(", ");
                }
                player.sendMessage(pages.substring(0, pages.length() - 2));
                return true;
            }
            if (!plugin.getConfig().getBoolean("hidePageCount", false)) {
                player.sendMessage(ChatColor.YELLOW + "===Rules page " + page + "/" + (plugin.getRulesManager().size()) + "===");
            }
            this.readPage(player, page);
            if(plugin.getRulesManager().size() > 1 && !isLastPage(page)) {
                String nextPage = getNextPage(page);
                player.sendMessage(ChatColor.YELLOW + "To view the next page, use /rules " + nextPage);
            }
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Page is not a number while it has to be!");
            return true;
        } finally {
            this.lastCommand.put(player.getUniqueId(), System.currentTimeMillis());
        }
        return true;
    }

    private String getNextPage(String page) {
        List<String> keys = new ArrayList<>(plugin.getRulesManager().keySet());
        return keys.get(keys.indexOf(page) + 1);
    }

    private boolean isLastPage(String page) {
        List<String> keys = new ArrayList<>(plugin.getRulesManager().keySet());
        return keys.get(keys.size() - 1).equals(page);
    }

    private void readPage(Player player, String page) {
        for (String rule : plugin.getRulesManager().get(page)) {
            player.sendMessage(ChatColor.GREEN + ChatColor.translateAlternateColorCodes('&', rule));
        }
        if (!plugin.getUsersWhoReadRules().containsKey(player.getName())) {
            ArrayList<String> list = new ArrayList<>();
            plugin.getUsersWhoReadRules().put(player.getName(), list);
        }
        plugin.getUsersWhoReadRules().get(player.getName()).add(page);
    }
}
